<?php

class Disciplina extends Eloquent {

	public $timestamps = false;

	/**
	 * The database table used by the model.
	 *
	 * @var string
	 */
	protected $table = 'disciplinas';

	protected $fillable = array('codigo', 'nome', 'professor');
}
