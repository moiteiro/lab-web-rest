<?php 

class TaskTableSeeder extends Seeder {

    public function run()
    {
        DB::table('tasks')->delete();

        Task::create(array('title' => 'Finalizar slides do workshop', 'order' => 1, 'completed' => 'no'));
        Task::create(array('title' => 'Configurar as maquinas', 'order' => 2, 'completed' => 'no'));
    }
}

?>