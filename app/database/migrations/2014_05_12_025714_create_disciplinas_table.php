<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateDisciplinasTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		
		Schema::create('disciplinas', function(Blueprint $table)
		{
			$table->increments('id');
			$table->string('codigo');
			$table->string('nome');
			$table->string('professor');
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::table('disciplinas', function(Blueprint $table)
		{
			//
		});
	}

}
