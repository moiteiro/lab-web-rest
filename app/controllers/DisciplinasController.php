<?php

class DisciplinasController extends \BaseController {

	/**
	 * Display a listing of the resource.
	 *
	 * @return Response
	 */
	public function index()
	{
		return Disciplina::all();
	}


	/**
	 * Show the form for creating a new resource.
	 *
	 * @return Response
	 */
	public function create()
	{
		//
	}


	/**
	 * Store a newly created resource in storage.
	 *
	 * @return Response
	 */
	public function store()
	{
		$input = Input::json();


		return Disciplina::create(array(
			'codigo' => $input->get('codigo'),
			'nome' => $input->get('nome'),
			'professor' => $input->get('professor')
		));

	}


	/**
	 * Display the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function show($id)
	{
		return Disciplina::find($id);
	}


	/**
	 * Show the form for editing the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function edit($id)
	{
		//
	}


	/**
	 * Update the specified resource in storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function update($id)
	{
		$input = Input::all();

		$disciplina = Disciplina::find($id);
		$disciplina->codigo = $input['codigo'];
		$disciplina->nome = $input['nome'];
		$disciplina->professor = $input['professor'];
		$disciplina->save();
	}


	/**
	 * Remove the specified resource from storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function destroy($id)
	{
		$disciplina = Disciplina::find($id);
		$disciplina->delete();
	}


}
